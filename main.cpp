#include <iostream>
#include <utility>
#include <vector>

using namespace std;

struct Carriage {
    bool array[54];
};

class Train {
private:
    int m_number;
    string m_start;
    string m_end;
    int m_carriageNumber;

public:
    Train(int t_number, string t_start, string t_end, int t_carriageNumber)
            : m_number(t_number), m_start(std::move(t_start)), m_end(std::move(t_end)), m_carriageNumber(t_carriageNumber) { }
    Train(const Train& t)
            : m_number(t.m_number), m_start(t.m_start), m_end(t.m_end), m_carriageNumber(t.m_carriageNumber) { }

    void setNumber(int t_number) {
        m_number = t_number;
    }

    void setStart(const string &t_start) {
        m_start = t_start;
    }

    void setEnd(const string &t_end) {
        m_end = t_end;
    }

    virtual void setCarriageNumber(int t_carriageNumber) {
        m_carriageNumber = t_carriageNumber;
    }

    int getCarriageNumber() const {
        return m_carriageNumber;
    }

    virtual ~Train() = default;

    virtual void printData() {
        cout << "number: " << m_number << " start: " << m_start << " end: " << m_end << " carriageNumber: " << m_carriageNumber << endl;
    }

    Train& operator = (Train const &train) {
        m_number = train.m_number;
        m_start = train.m_start;
        m_end = train.m_end;
        m_carriageNumber = train.m_carriageNumber;
        return *this;
    }
};

class DynamicTrain : public Train {
private:
    vector<Carriage> m_carriageVec;

public:
    DynamicTrain(int t_number = 84, string t_start = "Odessa", string t_end = "Kyiv", int t_carriageNumber = 5)
            : Train (t_number, t_start, t_end, t_carriageNumber) {
        setCarriageNumber(t_carriageNumber);
    }

    virtual void setCarriageNumber(int t_carriageNumber) {
        Train::setCarriageNumber(t_carriageNumber);
        m_carriageVec.resize(t_carriageNumber);
    }

    void reserve(int carriage, int place) {
        changeReserve(carriage, place, true);
    }

    void unReserve(int carriage, int place) {
        changeReserve(carriage, place, true);
    }

    virtual void printData() {
        Train::printData();
        printReservedPlaces();
    }

    void printReservedPlaces() {
        cout << "Reserved places : \n";
        for (int i = 0; i < m_carriageVec.size(); i++) {
            for (int j = 0; j < 54; j++) {
                if (m_carriageVec[i].array[j])
                    cout << i << "-" << j << endl;
            }
        }
        cout << endl;
    }

    virtual ~DynamicTrain() = default;

    DynamicTrain& operator = (DynamicTrain const &train) {
        Train::operator=(train);
        m_carriageVec = train.m_carriageVec;
        return *this;
    }

    DynamicTrain operator + (DynamicTrain const &train) {
        DynamicTrain tempTrain = *this;
        tempTrain.m_carriageVec = m_carriageVec;
        tempTrain.m_carriageVec.insert(tempTrain.m_carriageVec.end(), train.m_carriageVec.begin(), train.m_carriageVec.end());
        return tempTrain;
    }

private:

    void changeReserve(int carriage, int place, bool statement) {
        if (carriage > getCarriageNumber() || place > 53)
            return;
        m_carriageVec.at(carriage).array[place] = statement;
    }
};

int main() {
    DynamicTrain train(60);
    train.reserve(2, 30);
    train.printData();

    DynamicTrain train1(120);
    train1.reserve(1, 30);
    train1.printData();

    DynamicTrain train2 = train + train1;
    train2.printData();

    train2 = train1;
    train2.printData();

    return 0;
}